﻿using System.Net.Http;
using System;
using System.Net.Http.Headers;
using SWAPI.Abstract;
using SWAPI.Concrete;
using SWAPI.Utility;

namespace SWAPI
{

	internal sealed class SteamHttpClient
	{
		private HttpClient httpClient { get; set; }
		private HttpResponseMessage responseMessage { get; set; }
		private HttpRequestConfigurations httpRequestConfigurations { get; set; }
		private IResponseParser responseParser { get; set; }

		public SteamHttpClient( String apiKey, DataFormat dataFormat )
		{
			httpRequestConfigurations = new HttpRequestConfigurations( apiKey, dataFormat );
			ConfigureHttpClient( );
			ConfigureResponseParser( );
		}

		public void SetEndpoint( String endpoint )
		{
			httpRequestConfigurations.EndPoint = endpoint;
		}

		public void SetParameters( String parameters )
		{
			httpRequestConfigurations.Parameters = parameters;
		}

		public T MakeSteamRequest<T>( bool requiresApiKey = true )
		{
			try
			{
				responseMessage = requiresApiKey
					? httpClient.GetAsync( httpRequestConfigurations.GetRequestString( ) ).Result
					: httpClient.GetAsync( httpRequestConfigurations.GetRequestStringKeyless( ) ).Result;
				if (responseMessage.IsSuccessStatusCode)
				{
					return responseParser.GetDataFromResponseMessage<T>(responseMessage);
				}
			}
			catch ( Exception ex )
			{
				// not handling the exception... this could be a problem.
			}
			return default( T );
		}

		private void ConfigureHttpClient( )
		{
			httpClient = new HttpClient( );
			httpClient.BaseAddress = new Uri( "http://api.steampowered.com/" );
			httpClient.DefaultRequestHeaders.Clear( );
			switch ( httpRequestConfigurations.DataFormat.ToString( ) )
			{
				case "&format=json":
					httpClient.DefaultRequestHeaders.Accept.Add(
						 new MediaTypeWithQualityHeaderValue( "application/json" ) );
					break;
				case "&format=xml":
					httpClient.DefaultRequestHeaders.Accept.Add(
						 new MediaTypeWithQualityHeaderValue( "application/xml" ) );
					break;
				default:
					httpClient.DefaultRequestHeaders.Accept.Add(
						 new MediaTypeWithQualityHeaderValue( "application/vdf" ) );
					break;
			}
		}

		private void ConfigureResponseParser( )
		{
			switch ( httpRequestConfigurations.DataFormat.ToString( ) )
			{
				case "&format=json":
					responseParser = new JsonResponseParser( );
					break;
				case "&format=xml":
					responseParser = new JsonResponseParser( );
					break;
				default:
					responseParser = new JsonResponseParser( );
					break;
			}
		}


	}
}
