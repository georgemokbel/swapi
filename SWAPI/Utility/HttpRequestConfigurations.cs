﻿using System;

namespace SWAPI.Utility
{
     internal sealed class HttpRequestConfigurations
     {
          public DataFormat DataFormat { get; private set; }
          public String ApiKey { get; private set; }
          public String ApiKeyParameter { get; private set; }

          public String EndPoint { get; set; }
          public String Parameters { get; set; }

          public HttpRequestConfigurations( String apiKey, DataFormat dataFormat )
          {
               ApiKey = apiKey;
               DataFormat = dataFormat;
               ApiKeyParameter = "?key=" + apiKey;
          }

          public String GetRequestString()
          {
               
               return EndPoint + ApiKeyParameter + Parameters + DataFormat;
          }

          public String GetRequestStringKeyless()
          {
               return EndPoint + Parameters + DataFormat;
          }
     }
}
