﻿using System;

namespace SWAPI
{
     internal sealed class DataFormat
     {
          private readonly String name;
          private readonly int value;

          public static readonly DataFormat JSON = new DataFormat( 1, "&format=json" );

          private DataFormat( int value, String name )
          {
               this.name = name;
               this.value = value;
          }

          public override string ToString()
          {
               return name;
          }

     }
}
