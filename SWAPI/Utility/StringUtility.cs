﻿using System;

namespace SWAPI.Utility
{
     internal static class StringUtility
     {
          private const int MAX_STEAM_IDS_PER_REQUEST = 100;

          public static String[] GetCommaDelimittedSteamIds( String [] steamIds )
          {
               int numberOfStringsRequired = GetNumberOfStringsRequired(steamIds.Length);

               String[] commaDelimittedSteamIdStrings = new String[numberOfStringsRequired];

               int startIndex = 0;
               int numberOfElementsToCopy = 100;
               int currentStringIndex = 0;

               while ( currentStringIndex < numberOfStringsRequired )
               {
                    String commaDelimittedSteamId = String.Join(",", steamIds, startIndex, numberOfElementsToCopy);
                    commaDelimittedSteamIdStrings[currentStringIndex] = commaDelimittedSteamId;
                    startIndex += numberOfElementsToCopy;
                    ++currentStringIndex;
                    numberOfElementsToCopy = steamIds.Length - numberOfElementsToCopy;
               }

               return commaDelimittedSteamIdStrings;
          }

	     public static String StripPunctuationFromAppName(String appName)
	     {
		     if (!String.IsNullOrEmpty(appName))
		     {
				 char [] appNameArray = appName.ToCharArray( );

				 appNameArray = Array.FindAll<char>( appNameArray, ( c => ( char.IsLetterOrDigit( c ) ) ) );

				 return new string(appNameArray);
		     }
		     return "";
	     }

          private static int GetNumberOfStringsRequired( int numberOfIds )
          {
               return (numberOfIds/MAX_STEAM_IDS_PER_REQUEST) + 1;
          }
     }
}
