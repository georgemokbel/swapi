﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SWAPI.Abstract;

namespace SWAPI.Concrete
{
     internal sealed class JsonResponseParser : IResponseParser
     {
          public T GetDataFromResponseMessage<T>(HttpResponseMessage responseMessage)
          {
               Task<String> responseContentTask = responseMessage.Content.ReadAsStringAsync();
               JObject jObject = JObject.Parse(responseContentTask.Result);
               if ( jObject != null )
               {
                    return GetDataFromJObject<T>(jObject);
               }
               return default(T);
          }

          private T GetDataFromJObject<T>( JObject jObject )
          {
               return jObject.Root.First.First.ToObject<T>();
          }
     }
}
