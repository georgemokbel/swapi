﻿using System;
using System.Collections.Generic;
using SWAPI.Models;
using SWAPI.Utility;

namespace SWAPI
{
	public class SteamWebInterface
	{
		private readonly SteamHttpClient client;

		public SteamWebInterface(String apiKey)
		{
			client = new SteamHttpClient(apiKey, DataFormat.JSON);
		}

		//  http://api.steampowered.com/ISteamNews/GetNewsForApp/v0002/?appid=440&count=3&maxlength=300&format=json
		public virtual AppNews GetNewsForApp(int appId, int numberOfNewsEntriesReturned = 3, int maxLengthPerNewsEntry = 300)
		{
			client.SetEndpoint("ISteamNews/GetNewsForApp/v0002/");
			client.SetParameters("?appid=" + appId + "&count=" + numberOfNewsEntriesReturned + "&maxlength=" +
			                     maxLengthPerNewsEntry);
			var appNews = client.MakeSteamRequest<AppNews>(false);
			return appNews;
		}

		public virtual AppNews GetNewsForApp(String appId, int numberOfNewsEntriesReturned = 3,
			int maxLengthPerNewsEntry = 300)
		{
			if (!String.IsNullOrEmpty(appId))
			{
				int applicationId = 0;
				if (Int32.TryParse(appId, out applicationId))
				{
					return GetNewsForApp(applicationId, numberOfNewsEntriesReturned, maxLengthPerNewsEntry);
				}
			}
			return null;
		}

		// http://api.steampowered.com/ISteamUserStats/GetGlobalAchievementPercentagesForApp/v0002/?gameid=440&format=xml
		public virtual AchievementPercentages GetGlobalAchievementPercentagesForApp(int appId)
		{
			client.SetEndpoint("ISteamUserStats/GetGlobalAchievementPercentagesForApp/v0002/");
			client.SetParameters("?gameid=" + appId);
			var achievementPercentages = client.MakeSteamRequest<AchievementPercentages>(false);
			return achievementPercentages;
		}

		public virtual AchievementPercentages GetGlobalAchievementPercentagesForApp(String appName)
		{
			if (!String.IsNullOrEmpty(appName))
			{
				var appId = AppId.Invalid;
				// int appId = Convert.ChangeType(AppId.Invalid, AppIdGe)
				Enum.TryParse(appName, true, out appId);
				if (appId != AppId.Invalid)
				{
					var appIdValue = (int) appId;
					return GetGlobalAchievementPercentagesForApp( appIdValue );
				}
			}
			return null;
		}

		// http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=XXXXXXXXXXXXXXXXXXXXXXX&steamids=76561197960435530
		public virtual PlayerSummaries GetPlayerSummaries(String steamId)
		{
			client.SetEndpoint("ISteamUser/GetPlayerSummaries/v0002/");
			client.SetParameters("&steamids=" + steamId);
			var playerSummaries = client.MakeSteamRequest<PlayerSummaries>();
			return playerSummaries;
		}

		public virtual PlayerSummaries GetPlayerSummaries(String[] steamId)
		{
			if (steamId.Length == 0)
			{
				return null;
			}

			if (steamId.Length <= 100)
			{
				String steamIds = String.Join(",", steamId);
				return GetPlayerSummaries(steamIds);
			}

			String[] commaDelimittedSteamIds = StringUtility.GetCommaDelimittedSteamIds(steamId);
			var playerSummaries = new PlayerSummaries();

			foreach (String steamIds in commaDelimittedSteamIds)
			{
				playerSummaries.Players.AddRange(GetPlayerSummariesPlayers(steamIds));
			}

			return playerSummaries.Players.Count == 0 ? null : playerSummaries;
		}

		public virtual PlayerSummaries GetPlayerSummaries(List<String> steamIdList)
		{
			return GetPlayerSummaries(steamIdList.ToArray());
		}

		protected virtual List<Player> GetPlayerSummariesPlayers(String steamId)
		{
			PlayerSummaries playerSummaries = GetPlayerSummaries(steamId);
			return playerSummaries.Players;
		}

		// http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&steamid=76561197960435530&relationship=friend
		public virtual FriendsList GetFriendsList(String steamId, String relationship = "friend")
		{
			if (relationship != "friend" || relationship != "all")
			{
				relationship = "friend";
			}
			client.SetEndpoint("ISteamUser/GetFriendList/v0001/");
			client.SetParameters("&steamid=" + steamId + "&relationship=" + relationship);
			var friendsList = client.MakeSteamRequest<FriendsList>();
			return friendsList;
		}

		// http://api.steampowered.com/ISteamUserStats/GetPlayerAchievements/v0001/?appid=440&key=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&steamid=76561197972495328
		public virtual PlayerAchievements GetPlayerAchievementsForApp(String steamId, int appId, String language = "English")
		{
			client.SetEndpoint("ISteamUserStats/GetPlayerAchievements/v0001/");
			client.SetParameters("&appid=" + appId + "&steamid=" + steamId + "&l=" + language);
			var playerAchievements = client.MakeSteamRequest<PlayerAchievements>();
			return playerAchievements;
		}

		public virtual PlayerAchievements GetPlayerAchievementsForApp(String steamId, String appId,
			String language = "English")
		{
			if (!String.IsNullOrEmpty(appId))
			{
				int applicationId = 0;
				if (Int32.TryParse(appId, out applicationId))
				{
					return GetPlayerAchievementsForApp(steamId, applicationId, language);
				}
			}
			return null;
		}

		// http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=440&key=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&steamid=76561197972495328
		public virtual PlayerStats GetCompletedPlayerAchievementsAndStatsForApp(String steamId, int appId,
			String language = "English")
		{
			client.SetEndpoint("ISteamUserStats/GetUserStatsForGame/v0002/");
			client.SetParameters("&appid=" + appId + "&steamid=" + steamId + "&l=" + language);
			var playerStats = client.MakeSteamRequest<PlayerStats>();
			return playerStats;
		}

		public virtual PlayerStats GetCompletedPlayerAchievementsAndStatsForApp(String steamId, String appId,
			String language = "English")
		{
			if (!String.IsNullOrEmpty(appId))
			{
				int applicationId = 0;
				if (Int32.TryParse(appId, out applicationId))
				{
					return GetCompletedPlayerAchievementsAndStatsForApp(steamId, applicationId, language);
				}
			}
			return null;
		}

		// http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=XXXXXXXXXXXXXXXXX&steamid=76561197960434622&format=json
		public virtual OwnedGames GetOwnedGames(String steamId, bool includePlayedFreeGames = true)
		{
			client.SetEndpoint("IPlayerService/GetOwnedGames/v0001/");
			client.SetParameters("&steamId=" + steamId + "&include_appinfo=1" + "&include_played_free_games" +
			                     includePlayedFreeGames);
			var ownedGames = client.MakeSteamRequest<OwnedGames>();
			return ownedGames;
		}

		// http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key=XXXXXXXXXXXXXXXXX&steamid=76561197960434622&format=json
		public virtual RecentlyPlayedGames GetRecentlyPlayedGames(String steamId, int maxNumberOfGamesReturned = 999)
		{
			client.SetEndpoint("IPlayerService/GetRecentlyPlayedGames/v0001/");
			client.SetParameters("&steamId=" + steamId + "&count=" + maxNumberOfGamesReturned);
			var recentlyPlayedGames = client.MakeSteamRequest<RecentlyPlayedGames>();
			return recentlyPlayedGames;
		}

		public virtual RecentlyPlayedGames GetRecentlyPlayedGames(String steamId, String maxNumberOfGamesReturned = "999")
		{
			if (!String.IsNullOrEmpty(maxNumberOfGamesReturned))
			{
				int numbeOfGamesToReturn = 0;
				if (Int32.TryParse(maxNumberOfGamesReturned, out numbeOfGamesToReturn))
				{
					return GetRecentlyPlayedGames(steamId, numbeOfGamesToReturn);
				}
			}
			return null;
		}
	}
}