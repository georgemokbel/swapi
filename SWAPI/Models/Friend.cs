﻿using Newtonsoft.Json;

namespace SWAPI.Models
{
     [JsonObject]
     public class Friend
     {
          public string SteamId { get; set; }
          public string Relationship { get; set; }
          [JsonProperty( "friend_since" )]
          public int FriendSince { get; set; }
     }
}
