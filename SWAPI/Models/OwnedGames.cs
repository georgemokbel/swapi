﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SWAPI.Models
{
     [JsonObject]
     public class OwnedGames
     {
          [JsonProperty("game_count")]
          public int NumberOfOwnedGames { get; set; }
          public List<Game> Games { get; set; }
     }
}
