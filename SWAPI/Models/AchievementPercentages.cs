﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SWAPI.Models
{
     [JsonObject]
     public class AchievementPercentages
     {
          public List<Achievement> Achievements { get; set; }
     }
}
