﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SWAPI.Models
{
     [JsonObject]
     public class RecentlyPlayedGames
     {
          [JsonProperty("total_count")]
          public int NumberOfRecentlyPlayedGames { get; set; }
          public List<Game> Games { get; set; }
     }
}
