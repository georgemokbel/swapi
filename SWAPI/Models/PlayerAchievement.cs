﻿using Newtonsoft.Json;

namespace SWAPI.Models
{
     [JsonObject]
     public class PlayerAchievement
     {
          public string ApiName { get; set; }
          public string Description { get; set; }
          
          public string Name { get; set; }
          public bool Achieved { get; set; }
     }

}
