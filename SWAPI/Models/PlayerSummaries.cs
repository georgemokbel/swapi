﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SWAPI.Models
{
     [JsonObject]
     public class PlayerSummaries
     {
          public List<Player> Players { get; set; }
     }
}
