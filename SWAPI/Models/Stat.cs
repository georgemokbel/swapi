﻿using Newtonsoft.Json;

namespace SWAPI.Models
{
     [JsonObject]
     public class Stat
     {
          public string Name { get; set; }
          public int Value { get; set; }
     }
}
