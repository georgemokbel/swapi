﻿using Newtonsoft.Json;

namespace SWAPI.Models
{
     [JsonObject]
     public class PlayerStatAchievement
     {

          public string Name { get; set; }
          public bool Achieved { get; set; }
     }
}
