﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SWAPI.Models
{
     [JsonObject]
     public class FriendsList
     {
          public List<Friend> Friends { get; set; }
     }
}
