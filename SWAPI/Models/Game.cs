﻿
using Newtonsoft.Json;

namespace SWAPI.Models
{
     [JsonObject]
     public class Game
     {

          public int AppId { get; set; }
          public string Name { get; set; }
          [JsonProperty("img_icon_url")]
          public string ImageIconUrl { get; set; }
          [JsonProperty("img_logo_url")]
          public string ImageLogoUrl { get; set; }
          [JsonProperty("playtime_forever")]
          public int TotalPlayTime { get; set; }
          [JsonProperty("playtime_2weeks")]
          public int? LastTwoWeeksPlayTime { get; set; }
          [JsonProperty("has_community_visible_stats")]
          public bool HasVisibleStats { get; set; }
     }
}
