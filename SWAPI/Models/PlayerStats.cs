﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SWAPI.Models
{
     [JsonObject]
     public class PlayerStats
     {
          public string SteamId { get; set; }
          public string GameName { get; set; }
          public List<Stat> Stats { get; set; }
          public List<PlayerStatAchievement> Achievements { get; set; }
     }
}
