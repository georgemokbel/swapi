﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SWAPI.Models
{
   
     [JsonObject]
     public class PlayerAchievements
     {
          public string SteamId { get; set; }
          public string GameName { get; set; }
          public List<PlayerAchievement> Achievements { get; set; }
          public bool Success { get; set; }
     }
}
