﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SWAPI.Models
{
     [JsonObject]
     public class AppNews
     {
          public int AppId { get; set; }
          public List<NewsItem> NewsItems { get; set; }
     }
}
