﻿using Newtonsoft.Json;

namespace SWAPI.Models
{
     [JsonObject]
     public class Achievement
     {
          public string Name { get; set; }
          public double Percent { get; set; }
     }
}
