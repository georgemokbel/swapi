﻿
using System.Net.Http;

namespace SWAPI.Abstract
{
     interface IResponseParser
     {
          T GetDataFromResponseMessage<T>(HttpResponseMessage httpResponseMessage);
     }
}
