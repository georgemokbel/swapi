SWAPI - Steam Web API built by George Mokbel. 

There are others available like it but I felt they were too bulky.

SWAPI seeks to simplify using the web interface provided by valve for developers.

By using this library you agree to the Valve Developer Community: Terms of Use
https://developer.valvesoftware.com/wiki/Valve_Developer_Community:Terms_of_use

Please take note of Third Party Legal Notices
https://developer.valvesoftware.com/wiki/Third_Party_Legal_Notices

Licensing: (Licensed LGPL-2.1) See LGPL-2.1.txt 

